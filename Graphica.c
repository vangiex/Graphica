/*
	Author : Er. Mohit Ukani & Er. Rahul Rathi
	Date :	2017

*/


#include<stdio.h>
#include<graphics.h>
#include<conio.h>
#include<dos.h>

// Welcome Screen
void chat()
{
	 int i,j;
	 char m[6][90]={"osama : hello ALPHA2 message from osama","kasab : kasab here say.","osama : kale burj khalifa parr plane crash karvanu che","kasab : ok uda denge sasuri building ko","osama : tussi dil khush kardita mera kasab","next day location : burj khalifa dubai"};
	 printf("\n\b\b\b\b\b\b\b\b\b");
	 for(i=0;i<6&&!kbhit();i++)
	 {

		  for(j=0;m[i][j]!='\0'&&!kbhit();j++)
		  {
		   printf("%c",m[i][j]);
		   delay(200);
		  }
		  delay(300);
		  while(j>0)
		  {
			  printf("\b ");
			  printf("\b\b ");
			  delay(50);
		  	  j--;
		  }
	 }
}

// Drawing Static Scene
void building()
{
   int left,j,c,i, top, right, bottom;
   left = getmaxx()-70;
   top = getmaxy() / 2 - 50;
   right = getmaxx();
   bottom = getmaxy();
   line(left,top+70,(left+right)/2,top-80);
   line(right,top+70,(left+right)/2,top-80);
   line((left+right)/2,top-80,(left+right)/2,top-160);
   rectangle(left,top+70,right,bottom);
   left=0;
   right=75;
   top=250;
   for(i=0;i<=getmaxx()-140;i=i+75)
   {
	    rectangle(left+i+20,top+i/3,right+i,bottom);
	    c=((left+i+20)+(right+i))/2;
	    for(j=i/3;j<getmaxy()-20;j=j+5)
	    {
	     rectangle(c-17,top+j+5,c-7,top+j+10);
	     rectangle(c+7,top+j+5,c+17,top+j+10);
	    }
   }
}

// Animation & The Core Actor
void plane(int x,int y)
{
	line(x,y,x+10,y);
	line(x,y,x+20,y+20);
	line(x+20,y+20,x+120,y+20);
	line(x+90,y+10,x+120,y+20);
	line(x+90,y+15,x+105,y+15);
	line(x+90,y+15,x+82,y+10);
	line(x+10,y,x+20,y+10);
	line(x+20,y+10,x+90,y+10);
	line(x+30,y,x+46,y+10);
	line(x+30,y,x+40,y);
	line(x+40,y,x+58,y+10);
	line(x+50,y+15,x+60,y+15);
	line(x+30,y+30,x+50,y+15);
	line(x+30,y+30,x+40,y+30);
	line(x+40,y+30,x+60,y+15);
}

// Action's of Actor
void planer(int x,int y)
{
	line(x,y,x-10,y);
	line(x,y,x-20,y+20);
	line(x-20,y+20,x-120,y+20);
	line(x-90,y+10,x-120,y+20);
	line(x-90,y+15,x-105,y+15);
	line(x-90,y+15,x-82,y+10);
	line(x-10,y,x-20,y+10);
	line(x-20,y+10,x-90,y+10);
	line(x-30,y,x-46,y+10);
	line(x-30,y,x-40,y);
	line(x-40,y,x-58,y+10);
	line(x-50,y+15,x-60,y+15);
	line(x-30,y+30,x-50,y+15);
	line(x-30,y+30,x-40,y+30);
	line(x-40,y+30,x-60,y+15);
}


// Conclusion
void crash(int x,int y)
{
	line(x,y,x-10,y);
	line(x,y,x-20,y-20);
	line(x-20,y-20,x-50,y-20);
	line(x-85,y-20,x-120,y-20);
	line(x-90,y-10,x-120,y-20);
	line(x-90,y-15,x-105,y-15);
	line(x-90,y-15,x-82,y-10);
	line(x-10,y,x-20,y-10);
	line(x-20,y-10,x-45,y-10);
	line(x-80,y-10,x-90,y-10);
	line(x-30,y,x-46,y-10);
	line(x-30,y,x-40,y);
	line(x-40,y,x-58,y-10);
	line(x-50,y-15,x-60,y-15);
	line(x-30,y-30,x-50,y-15);
	line(x-30,y-30,x-40,y-30);
	line(x-40,y-30,x-60,y-15);
}

// Static Scene
void cloud(int x, int y)
{       int i;
	for(i=0;i<20;i++)
	{
		setcolor(WHITE);
		circle(x,y,20-i);
		circle(x+30,y+10,20-i);
		circle(x+30,y-10,20-i);
		circle(x+50,y,20-i);
	}
}


// Driver Part
void main()
{
	int gdriver = DETECT, gmode,i,j,x=0;
	initgraph(&gdriver, &gmode, "C:\\TC\\bgi");
	cloud(getmaxx()/2-100,50);
	cloud(getmaxx()/2+50,75);
	for(i=0;i<=20;i++){
		setcolor(YELLOW);
		circle(getmaxx()-100,75,20-i);
	}
	for(i=0;i<=20;i++){
		setcolor(BLACK);
		circle(getmaxx()-110,65,20-i);
		circle(getmaxx()-109,65,20-i);
	}

	printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\t\t       hello guys welcome!!!");
	delay(300);
	for(i=0;i<getmaxx()&&!kbhit();i++)
	{
	 plane(0+i,getmaxy()/2-20);
	 planer(getmaxx()-i,getmaxy()/2-20);
	 delay(10);
	 setcolor(BLACK);
	 plane(0+i,getmaxy()/2-20);
	 planer(getmaxx()-i,getmaxy()/2-20);
	 setcolor(WHITE);
	 if(i==(getmaxx()/3*2)+70){
		printf("\n\t\tthis is a short film on 9-11 in dubai burj al arab");
	 }
	}
	delay(100);
	for(i=0;i<getmaxx()&&!kbhit();i++){
		plane(0+i,getmaxy()/2-15);
		planer(getmaxx()-i,getmaxy()/2-15);
		delay(10);
		setcolor(BLACK);
		plane(0+i,getmaxy()/2-15);
		planer(getmaxx()-i,getmaxy()/2-15);
		setcolor(WHITE);
	}
	cleardevice();
	chat();
	cleardevice();
	setcolor(WHITE);
	building();
	cloud(getmaxx()/2-300,60);
	cloud(getmaxx()/2+50,75);
	for(i=0;i<=20;i++){
		setcolor(YELLOW);
		circle(getmaxx()-100,75,20-i);
	}
	for(i=0;i<=20;i++){
		setcolor(BLACK);
		circle(getmaxx()-110,65,20-i);
		circle(getmaxx()-109,65,20-i);
	}
	for(i=0;i<=getmaxx()-170&&!kbhit();i++)
	{
		sound(1100-i);
		setcolor(WHITE);
		plane(0+i,90+i/4);
		delay(30);
		setcolor(BLACK);
		plane(0+i,90+i/4);
	}
	for(j=0;(90+i/4+j)<getmaxy()-20&&!kbhit();j+=5)
	{       sound(2000+x);
		setcolor(WHITE);
		crash(i+160,90+i/4+j);
		x=x+10;
		delay(40);
		sound(2500+x);
		setcolor(BLACK);
		crash(i+160,90+i/4+j);
		if(x==100){x=0;}
	}
	nosound();
	clrscr();
	cleardevice();
	cloud(getmaxx()/2-100,30);
	cloud(getmaxx()/2+50,75);
	for(i=0;i<=20;i++){
		setcolor(YELLOW);
		circle(getmaxx()-100,75,20-i);
	}
	for(i=0;i<=20;i++){
		setcolor(BLACK);
		circle(getmaxx()-110,65,20-i);
		circle(getmaxx()-109,65,20-i);
	}
	setcolor(WHITE);
	outtextxy(0,getmaxy()/2-50,"YE BUILDIING AMBUJA CEMENT SE BANIH HE IS CEMENT ME JAAN HE");
	building();
	delay(10000);
	cleardevice();
	cloud(getmaxx()/2-100,30);
	cloud(getmaxx()/2+50,75);
	for(i=0;i<=20;i++){
		setcolor(YELLOW);
		circle(getmaxx()-100,75,20-i);
	}
	for(i=0;i<=20;i++){
		setcolor(BLACK);
		circle(getmaxx()-110,65,20-i);
		circle(getmaxx()-109,65,20-i);
	}
	setcolor(WHITE);
	outtextxy(getmaxx()/2-160,getmaxy()/2,"prepared by mohit & rahul!!!");
	delay(300);
	for(i=0;i<getmaxx()&&!kbhit();i++)
	{
	 plane(0+i,getmaxy()/2-20);
	 planer(getmaxx()-i,getmaxy()/2-20);
	 delay(20);
	 setcolor(BLACK);
	 plane(0+i,getmaxy()/2-20);
	 planer(getmaxx()-i,getmaxy()/2-20);
	 setcolor(WHITE);
	 if(i==(getmaxx()/3*2)+70)
	 {
	 outtextxy(getmaxx()/2-60,getmaxy()/2,"thank you");
	 }
	}
	delay(100);
	for(i=0;i<getmaxx()&&!kbhit();i++)
	{
	 plane(0+i,getmaxy()/2-15);
	 planer(getmaxx()-i,getmaxy()/2-15);
	 delay(20);
	 setcolor(BLACK);
	 plane(0+i,getmaxy()/2-15);
	 planer(getmaxx()-i,getmaxy()/2-15);
	 setcolor(WHITE);
	}
	getch();
	closegraph();
}

